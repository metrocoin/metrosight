'use strict';

angular.module('metrosight',[
  'ngAnimate',
  'ngResource',
  'ngRoute',
  'ngProgress',
  'ui.bootstrap',
  'ui.route',
  'monospaced.qrcode',
  'gettext',
  'metrosight.system',
  'metrosight.socket',
  'metrosight.blocks',
  'metrosight.transactions',
  'metrosight.address',
  'metrosight.search',
  'metrosight.status',
  'metrosight.connection',
  'metrosight.currency'
]);

angular.module('metrosight.system', []);
angular.module('metrosight.socket', []);
angular.module('metrosight.blocks', []);
angular.module('metrosight.transactions', []);
angular.module('metrosight.address', []);
angular.module('metrosight.search', []);
angular.module('metrosight.status', []);
angular.module('metrosight.connection', []);
angular.module('metrosight.currency', []);
