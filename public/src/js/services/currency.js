'use strict';

angular.module('metrosight.currency').factory('Currency',
  function($resource) {
    return $resource('/api/currency');
});
